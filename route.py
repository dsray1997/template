def filter(val):
    filteredStr = ""
    for j in val:
        if (j >= 'A' and j<='Z') or (j>='a' and j<='z') or j=='_':
            filteredStr += j
    return(filteredStr)

routeName = input("Enter route's name : ")

f = open("input.txt", "r")
procName = ""
params = []
for i in f:
    lis = i.split(" ")
    if "PROCEDURE" in lis:
        temp = lis[lis.index("PROCEDURE") + 1]
        for j in temp:
            if (j >= 'A' and j<='Z') or (j>='a' and j<='z') or j=='_':
                procName += j
    else:
        tempLis = []
        for k in lis:
            if(k != ''):
                tempLis.append(k)
        if("in" in tempLis):
            params.append(filter(tempLis[tempLis.index("in") + 1]))
        elif("IN" in tempLis):
            params.append(filter(tempLis[tempLis.index("IN") + 1]))
        elif("In" in tempLis):
            params.append(filter(tempLis[tempLis.index("In") + 1]))

ans = "Route::post('/" + routeName +"', function (Request $request)  {\n"

fileTypes = []


count = 0
for i in params:
    print(count, ": ", i)
    count+=1

if int(input("Are there any parameters of file type: (1 for yes 0 for no)")):

    print("\nGive the indices of the file type parameters.(Comma separated ex: 0,5,6)")
    fileTypes = list(input().split(","))

x = 0
for i in params:
    if str(x) in fileTypes:
        ans += "\n\t$" + i + " = $request->file('" + i + "')->store('EDIT THIS');\n "
        ans += "\t$" + i + "=$path;\n\n "
    else:
        ans += "\t$" + i + " = $request->input('" + i + "');\n"
    x+=1

ans += "\n\t$res = DB::select ('call " + procName +"(";
for i in range(len(params)-1):
    ans += "?, "

ans +="?)', \n"
ans +="\tarray("

count = 1
for i in params:
    if(count % 5 == 0):
        count = 0
        ans += "\n\t\t"
    ans += "$" + i + ", "
    count += 1

ans = ans[:-2]
ans += "));\n\n"

ans += "\treturn $res;\n";

ans += "});"


g = open("output.txt", "w")
g.write(ans)
print("Result stored in output.txt!")
g.close()
f.close()
