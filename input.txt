PROCEDURE `add_exam_answers`
(
IN `question_i_d` BIGINT,
 IN `student_i_d` BIGINT, 
IN `answer_marked` VARCHAR(100), 
IN `answer_marked_at` DATETIME,
 IN `question_color` VARCHAR(20)
)
